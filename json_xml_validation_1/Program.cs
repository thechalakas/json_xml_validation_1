﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace json_xml_validation_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //first I need a json response

            //I cannot use await calls in the main method. so using another method for that

            string sample_json = demo_async_await().Result;

            Console.WriteLine("The json returned is {0}", sample_json);

            //so, I have a json response from that link

            //lets deserialize this json object now
            //note that the reference needed for this is not readily availalbe in the default console application
            //check notes on how to get this reference
            
            

            var temp_serializer = new JavaScriptSerializer();

            //I am going to put this in a try block in case the json is invalid
            try
            { 
                var deserialize_temp_string = temp_serializer.Deserialize<Dictionary<string, object>>(sample_json);
            }
            catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //lets not let the console dissapear
            Console.ReadLine();

        }

        static async Task<string> demo_async_await()
        {
            string result_of_response = "";


            HttpClient client = new HttpClient();
            

            //the await keyword indicates that this activity will take a while
            //if the following is done, then the code just continues
            //if the following is done, it will take a while, in which case, control is returned
            //to the main thread which can do other stuff

            //wikipedia has an open api that anybody can use
            //sure there are other APIs but they all need some registration and other time wasters

            try
            {
                result_of_response = await client.GetStringAsync("https://en.wikipedia.org/w/api.php?action=query&titles=Main%20Page&prop=revisions&rvprop=content&format=json");
            
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            
            return result_of_response;
        }
    }
}
